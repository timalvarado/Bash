#!/bin/bash
# Used this script to find all users on a hosted webserver that used dynamic content

declare -a arr=("user1"
		"user2"
)

for i in "${arr[@]}"
do
	path=$(find /path/to/search -type d -name "$i")
	find $path -type f -name '*.cgi'
	find $path -type f -name '*.php'
	find $path -type f -name '*.py'
done
